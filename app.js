const car = (name, model, owner, year, phone, image) => ({name, model, owner, year, phone, image});
const log = (text, type, date = new Date) => ({text, type, date})
const cars = [
   car('Ford', 'Focus', 'Max', 2015, '+375445852229', 'img/ford.jpg'),
   car('Ford', 'Pocus', 'Sax', 2013, '+275423322229', 'img/ford1.jpg'),
   car('Porshe', 'Cocus', 'Fax', 2019, '+175412352229', 'img/ford3.jpg')
]

new Vue({
   el: '#app',
   data: {
      cars: cars,
      car: cars[0],
      logs: [],
      selectCarIndex: 0,
      phoneVisibility: false,
      search: '',
      modalVisibility: false
   },
   methods: {
      selectCar(index) {  
         this.selectCarIndex = index;
         this.car = this.filteredCars[this.selectCarIndex]
      },
      newOrder() {
         this.modalVisibility = false
         this.logs.push(
            log(`Success order: ${this.car.name} - ${this.car.model}`, 'ok')
         )
      },
      cancelOrder() {
         this.modalVisibility = false
         this.logs.push(
            log(`Cancelled order: ${this.car.name} - ${this.car.model}`, 'cnl')
         )
      }
   },
   computed: {
      phoneBtnText() {
         return this.phoneVisibility ? 'Hide phone' : 'Show Phone'
      },
      filteredCars() {
         return this.cars.filter( (car) => {
            return car.name.indexOf(this.search) > -1 || car.model.indexOf(this.search) > -1
         })          
      }
   },
   filters: {
      date(value) {
         return value.toLocaleString()
      }
   }
})